import javax.sound.midi.*; //Importing the midi library so java can play sounds.


public class MiniMiniMusicCmdLine {   //This class holds the methods for playing a sound.
     public static void main(String[] args) {
        MiniMiniMusicCmdLine mini = new MiniMiniMusicCmdLine();
        if (args.length < 2) {
            System.out.println("Don't forget the instrument and note args"); //When less than two arguments are given by the user, this if statement will run and print a suggestion.
        } else {
            int instrument = Integer.parseInt(args[0]);
            int note = Integer.parseInt(args[1]);
            for (int x=0; x<120; x++){
              mini.play(instrument, note); //This calls the play method, the method that actually plays a sound.
              instrument++; //This is the big change. It incriments through intruments so that every time midi.play is called, a new instrument is used.
            }
            System.exit(0);

        }
     }

    public void play(int instrument, int note) { //This is the method used to play a sound. Every time this method is called, java will play a sound.

      try {

         Sequencer player = MidiSystem.getSequencer();
         player.open();

         Sequence seq = new Sequence(Sequence.PPQ, 4);
         Track track = seq.createTrack();

         MidiEvent event = null;

         ShortMessage first = new ShortMessage();
         first.setMessage(192, 1, instrument, 0);
         MidiEvent changeInstrument = new MidiEvent(first, 1);
         track.add(changeInstrument);


         ShortMessage a = new ShortMessage();
         a.setMessage(144, 1, note, 100);
         MidiEvent noteOn = new MidiEvent(a, 1);
         track.add(noteOn);

         ShortMessage b = new ShortMessage();
         b.setMessage(128, 1, note, 100);
         MidiEvent noteOff = new MidiEvent(b, 16);
         track.add(noteOff);
         player.setSequence(seq);
         player.start();
	       Thread.sleep(70);


      } catch (Exception ex) {ex.printStackTrace();}
  } // close play

} // close class
