public class SodapopSong {
       public static void main (String[] args) {
         int sodapopNum = 99;
         String word = "bottles";
         while (sodapopNum > 0) {
            if (sodapopNum == 1) {
            } word = "bottle"; // singular, as in ONE bottle.
            System.out.println(sodapopNum + " " + word + " of sodapop on the wall");
            System.out.println(sodapopNum + " " + word + " of sodapop.");
            System.out.println("Take one down.");
            System.out.println("Pass it around.");
            sodapopNum = sodapopNum - 1;
            if (sodapopNum > 0) {
               System.out.println(sodapopNum + " " + word + " of sodapop on the wall");
            } else {
               System.out.println("No more bottles of sodapop on the wall");
            } // end else
         } // end while loop
  } // end main method
} // end class
